import React from 'react';
import { defaultCategory } from '../../constant';

const ProductCard = ({product: {id, title}}) => <h4 key={id}>{title}</h4>;

export default function ProductList({products, category}) {
  const isDefaultCategory = category === defaultCategory;
  const filteredProducts = isDefaultCategory ? products : products.filter(p => p.category === category);

  return <div className='product-list'>
    {filteredProducts.map(product => <ProductCard product={product}/>)}
  </div>;
}