import React from "react";
import { defaultCategory } from '../../constant';
import './style.css';

export default function CategoryList({categories, category, onChange}) {
  const isDefaultCategory = category === defaultCategory;

  return <ul className="category-list">
    <li className={isDefaultCategory ? 'is-active' : ''}>
      <button onClick={isDefaultCategory ? () => {} : () => onChange(defaultCategory)}>Все</button>
    </li>
    {categories.map(c => {
      const isActive = category === c;
      return <li key={c} className={isActive ? 'is-active' : ''}>
        <button onClick={isActive ? () => {} : () => onChange(c)}>{c}</button>
      </li>;
    })}
  </ul>;
}