import React from 'react';
import ProductList from "./components/ProductList";
import CategoryList from "./components/CategoryList";
import productsMockData from './mock/products.json';
import { defaultCategory } from './constant';
import './app.css';

function App() {
  const [category, setCategory] = React.useState(defaultCategory);
  const [categories, setCategories] = React.useState([]);
  const [products, setProducts] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    setLoading(true);
    const categories = Array.from(new Set(productsMockData.map(product => product.category)));

    setTimeout(() => {
      setCategories(categories);
            setProducts(productsMockData);
      setLoading(false);
    }, 1000);
    
  }, []);

  if(loading) {
    return <span>загрузка продуктов ...</span>;
  }

  if (!products.length || !categories.length) {
    return <span>ops... данные потерялись</span>;
  }

  const onChangeCategory = category => setCategory(category);

  return (
    <div className="app">
      <CategoryList category={category} onChange={onChangeCategory} categories={categories}/>
      <ProductList category={category} products={products}/>
    </div>
  );
}

export default App;
